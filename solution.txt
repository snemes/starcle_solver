
Solution found in 0.14s:
1. Rotate left disc counter-clockwise 3 times
2. Rotate right disc counter-clockwise 2 times
3. Rotate left disc clockwise
4. Rotate right disc clockwise 2 times
5. Rotate left disc counter-clockwise
6. Rotate right disc counter-clockwise
7. Rotate left disc counter-clockwise 3 times
8. Rotate right disc clockwise 3 times
9. Rotate left disc clockwise
10. Rotate right disc clockwise 3 times
11. Rotate left disc counter-clockwise 4 times
12. Rotate right disc clockwise
13. Rotate left disc counter-clockwise
14. Rotate right disc clockwise
15. Rotate left disc clockwise
16. Rotate right disc counter-clockwise 2 times
17. Rotate left disc counter-clockwise
18. Rotate right disc counter-clockwise 3 times
19. Rotate left disc clockwise
20. Rotate right disc counter-clockwise
21. Rotate left disc counter-clockwise 2 times
22. Rotate right disc counter-clockwise
23. Rotate left disc counter-clockwise 2 times
24. Rotate right disc clockwise 2 times
25. Rotate left disc counter-clockwise
26. Rotate right disc counter-clockwise
27. Rotate left disc counter-clockwise
28. Rotate right disc clockwise
29. Rotate left disc clockwise
30. Rotate right disc clockwise
31. Rotate left disc counter-clockwise 2 times
32. Rotate right disc clockwise
33. Rotate left disc clockwise
34. Rotate right disc clockwise 2 times
35. Rotate left disc counter-clockwise
36. Rotate right disc counter-clockwise
37. Rotate left disc clockwise 2 times
38. Rotate right disc counter-clockwise
39. Rotate left disc counter-clockwise
40. Rotate right disc counter-clockwise
41. Rotate left disc clockwise
42. Rotate right disc clockwise
43. Rotate left disc counter-clockwise
44. Rotate right disc counter-clockwise
45. Rotate left disc counter-clockwise
46. Rotate right disc clockwise
47. Rotate left disc clockwise
