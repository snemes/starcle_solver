#!/usr/bin/python -O
#
# Sandor Nemes, 2015.
#
# Heuristics solver for the Starcle puzzle game (http://www.sntapps.com/starcle)
#
#
# TODO: calculate number of combinations? 10 triangles, 19 petals
# TODO: multiple threads?
# TODO: good heuristics? proper position, element neightborhood?

import time
import random


#import collections

# tile colors
GRAY    = 0
RED     = 1
YELLOW  = 2
CYAN    = 3
MAGENTA = 4
BLUE    = 5

# disc names
LEFT    = 0
RIGHT   = 1

# directions: DOWN = CCW, UP = CW
DOWN    = 0
UP      = 1
CCW     = 0
CW      = 1


def check(state):
    try:
        assert state[0][2] == state[1][15]
        assert state[0][3] == state[1][17]
        assert state[0][4] == state[1][16]
        assert state[0][5] == state[1][14]
        assert state[0][6] == state[1][11]
        assert state[0][7] == state[1][13]
        assert state[0][8] == state[1][12]
    except AssertionError:
        print(state)
        raise


def sync(state, disc):
    if disc == LEFT:
        state[0][2] = state[1][15]
        state[0][3] = state[1][17]
        state[0][4] = state[1][16]
        state[0][5] = state[1][14]
        state[0][6] = state[1][11]
        state[0][7] = state[1][13]
        state[0][8] = state[1][12]
    elif disc == RIGHT:
        state[1][15] = state[0][2]
        state[1][17] = state[0][3]
        state[1][16] = state[0][4]
        state[1][14] = state[0][5]
        state[1][11] = state[0][6]
        state[1][13] = state[0][7]
        state[1][12] = state[0][8]
    else:
        raise RuntimeError


def heu(state, goal):
    scoreA = 0
    scoreB = 0
    scoreC = 0

    for i in [LEFT, RIGHT]:

        # heuristic score #1
        for j in range(0, 6):
            for k in range(0, 6):
                if goal[i][3*j:3*j+3] == state[i][3*k:3*k+3]:
                    scoreA = scoreA + 1

        # heuristic score #2
        for j in range(0, 18):
                if goal[i][j] == state[i][j]:
                    scoreB = scoreB + 1

        # heuristic score #3
        for j in range(0, 6):
                if goal[i][3*j:3*j+3] == state[i][3*j:3*j+3]:
                    scoreC = scoreC + 1

    return 10*scoreA + scoreB #+ int(10*random.random())


def move(state, disc, rotation):
    check(state)
    if disc == LEFT:
        result = [state[LEFT][-3*rotation:] + state[LEFT][:-3*rotation], state[RIGHT][:]]
        sync(result, RIGHT)
    elif disc == RIGHT:
        result = [state[LEFT][:], state[RIGHT][-3*rotation:] + state[RIGHT][:-3*rotation]]
        sync(result, LEFT)
    else:
        raise RuntimeError
    check(result)
    return result


def solve(state1, state2):
    result = None
    pos = 0
    queue = [{
        'state': state1,
        'processed': False,
        'step': None,
        'score': heu(state1, state2)
    }]

    if __debug__:
        print('Start: ' + str(state1))
        print('Goal : ' + str(state2))

    print('Searching...')
    if state1 == state2: return []

    while True:
        node = None
        first = -1
        last = -1
        count = 0
        total = len(queue)
        maxscore = -1

        for i in xrange(total):
            if not queue[i]['processed']:
                if first < 0: first = i
                last = i
                count = count + 1
                if maxscore < 0 or queue[i]['score'] > queue[maxscore]['score']:
                    maxscore = i

        if __debug__:
            print('{0}% ({1}/{2}), MAX = {3}'.format(100 * (total - count) / total, (total - count), total, queue[maxscore]['score']))

        selected = maxscore

        try:
            if selected < 0: raise IndexError
            node = queue[selected]
        except IndexError:
            break

        for disc in (LEFT, RIGHT):
            for rotation in (-2, -1, 1, 2, 3):
                newstate = move(node['state'], disc, rotation)
                step = (selected, { 'disc': disc, 'rotation': rotation })
                if newstate == state2:
                    result = []
                    n = step
                    while n is not None:
                        result.append(n[1])
                        n = queue[n[0]]['step']
                    result.reverse()
                    return result
                exists = False
                for n in queue:
                    if newstate == n['state']:
                        exists = True
                        break
                if not exists:
                    queue.append({
                        'state': newstate,
                        'processed': False,
                        'step': step,
                        'score': heu(newstate, state2)
                    })

        node['processed'] = True

    return None


def main():
    start = [
        [
            BLUE, MAGENTA, GRAY,
            GRAY, GRAY, GRAY,
            GRAY, MAGENTA, GRAY,
            BLUE, GRAY, GRAY,
            BLUE, GRAY, BLUE,
            BLUE, GRAY, GRAY
        ],[
            BLUE, MAGENTA, GRAY,
            BLUE, GRAY, BLUE,
            BLUE, GRAY, GRAY,
            BLUE, GRAY, GRAY,
            GRAY, MAGENTA, GRAY,
            GRAY, GRAY, GRAY
        ]
    ]
    check(start)

    goal = [
        [
            BLUE, GRAY, BLUE,
            BLUE, MAGENTA, GRAY,
            BLUE, GRAY, BLUE,
            BLUE, MAGENTA, BLUE,
            GRAY, GRAY, GRAY,
            GRAY, GRAY, GRAY
        ],[
            BLUE, GRAY, GRAY,
            GRAY, GRAY, GRAY,
            GRAY, GRAY, BLUE,
            BLUE, MAGENTA, BLUE,
            BLUE, GRAY, GRAY,
            BLUE, MAGENTA, BLUE
        ]
    ]
    check(goal)

    start_time = time.time()
    solution = solve(start, goal)
    end_time = time.time()

    if solution is not None:
        print('\nSolution found in {:.2f}s:'.format(end_time - start_time))
        for i in xrange(len(solution)):
            print('{}. Rotate {} disc {}'.format(
                i + 1,
                ('left', 'right')[solution[i]['disc']],
                {
                    -2: 'counter-clockwise 2 times',
                    -1: 'counter-clockwise',
                    1: 'clockwise',
                    2: 'clockwise 2 times',
                    3: 'clockwise 3 times'
                }[solution[i]['rotation']],
            ))
    else:
        print('Exhausted state space, solution not found.')


if __name__ == "__main__":
    main()
